__kernel void 
_ZN9myFunctor19__cxxamp_trampolineEPU3AS1fjS1_jS1_j(
	    __global const float *a, unsigned sz_a,
            __global const float *b, unsigned sz_b,
            __global float *c, unsigned sz_c)
{
    unsigned i = get_global_id(0);
    if(i >= sz_a) return;

    c[i] = a[i] + b[i];
}

